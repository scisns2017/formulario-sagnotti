\documentclass[a4paper]{article}

\usepackage{style}
\usepackage{fullpage}

\title{Formulario per il corso di Meccanica Statistica}
\author{Niccolò Porciani, Alessandro Piazza, Marco Venuti}

\begin{document}
\begin{titlepage}
  \maketitle
  \begin{center}
    \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{\includegraphics[scale = 0.8]{by-nc-sa}}
  \end{center}
  \tableofcontents

\end{titlepage}

\section{Matematica}

\subsection{Integrali gaussiani}
\[
  \int_{\R} e^{-\alpha x^{2} + i \beta x} \dif{x} = \sqrt{\frac{\pi}{\alpha}} e^{-\frac{\beta^{2}}{4 \alpha}},\quad \alpha > 0, \beta\in\R
\]

\subsection{Baker-Campbell-Hausdorff}
\[
  e^{i \lambda \hat{B}}\hat{A} e^{-i \lambda \hat{B}}
  = \sum_{k=0}^{+\infty} \frac{{(i \lambda)}^{k}}{k!} \overbrace{[\hat{B}, [\hat{B}, [\cdots, [\hat{B},\hat{A}]]]]}^{k\text{ volte}}
  = \hat{A} + i \lambda [\hat{B},\hat{A}] - \frac{\lambda^{2}}{2} [\hat{B},[\hat{B},\hat{A}]] + \cdots
\]

\subsection{Funzione $ \Gamma $}

\[
  \Gamma(x) = \int_{0}^{+\infty} \dif t\ t^{x-1}\ e^{-t}
\]

\[ \Gamma(x+1) = x \Gamma(x) \]

\[
  \Gamma(N) \sim N! \sim \sqrt{2 \pi N} \frac{N^N}{e^N} \qquad \del{N\to\infty}
\]

\[
  \ln\del{\Gamma(N)} \sim \log{\del{N!}} \sim N \ln N - N \qquad \del{N\to\infty}
\]

\[
  \Omega_d = \frac{2 \pi^{d/2}}{\Gamma \del{\frac{d}{2}}}
\]

\subsection{Funzioni di Bessel modificate $ K_{\nu} $}
\[
  K_{\nu}(z) = z^{\nu} \int_{1}^{+\infty} (x^{2} - 1)^{z - \frac{2}{2}} e^{-zx} \dif{x} \simeq
  \begin{dcases}
    \left(\frac{2}{z}\right)^{\nu} \frac{\Gamma(\nu)}{2} \left[1 + O(z^{2})\right] & \text{per } z \ll 1 \\
    \sqrt{\frac{\pi}{2z}} e^{-z} \left[1 + \frac{4\nu^{2} - 1}{8z} + O\left(\frac{1}{z^{2}}\right)\right] & \text{per } z \gg 1
  \end{dcases}
\]

\subsection{Funzione $ \beta $ generalizzata}
\[
  \int_{0}^{\infty} x_{1}^{n_{1}-1} \cdots x_{N}^{n_{N}-1} \delta(x_{1} + \cdots x_{N} - 1) \dif{x_{1}} \cdots \dif{x_{N}}
  = \frac{\Gamma(n_{1}) \cdots \Gamma(n_{N})}{\Gamma(n_{1} + \cdots + n_{N})}
\]

\subsection{Metodi di approssimazione}

\begin{itemize}
\item \textbf{Metodo di Laplace:}
  \[
    \int_{a}^{b} f(t) e^{x\phi(t)} \dif t \sim
    f(c) e^{x \phi(c)} \sqrt{\frac{\pi}{-\frac{1}{2} x\phi''(c)}}
    \qquad \del{x \to +\infty}
  \]
  se $ c $ è un minimo assoluto di $ \phi $ interno ad $ (a,b) $

\item \textbf{Metodo di Laplace generalizzato:}
  \[
    \int_{a}^{b} f(t) e^{x\phi(t)} \dif t \sim
    \int_{a}^{b} f(t) e^{x \sbr{\phi (c) + \frac{1}{2} \phi''(c) (x-c)^2}} \dif t
    \qquad \del{x \to +\infty}
  \]
  sostituendo $ f(c) $ a $ f(t) $ ci si riconduce al metodo di Laplace. Il Bender - Orzag sostiene che funzioni sempre se $ f $ va a zero algebricamente (come una potenza, credo)

\item \textbf{Metodo della fase stazionaria:}
  \[
    \int_a^b f(t) e^{i x \psi(t)} \dif t \sim
    f(a)
    e^{i x \psi(a) +
      \operatorname{sgn}\del{\psi^{(p)} (a)} \frac{i \pi}{2 p}}
    \del{\frac{p!}{x \envert{\psi^{(p)} (a)}}}^{\frac{1}{p}}
    \frac{\Gamma\del{\frac{1}{p}}}{p}
    \qquad \del{x\to + \infty}
  \]
  se $ \psi $ è reale, $ \psi'(t) $ si annulla solo in $ a $ e $ \psi^{(p)} $ è la prima derivata non nulla in $ a $.\\
  Caso speciale $ p = 2 $
  \[
    \int_a^b f(t) e^{i x \psi(t)} \dif t \sim
    f(a)
    e^{i x \psi(a) +
      \operatorname{sgn}\del{\psi'' (a)} \frac{i \pi}{4}}
    \sqrt{\frac{\pi}{2 x \envert{\psi'' (a)}}}
    \qquad \del{x\to + \infty}
  \]

\item \textbf{Metodo della Steepest descent:}
  Fornisce il comportamento asintotico per $ x \to +\infty,\ x\in\R $ di integrali della forma
  \[
    \int_{\gamma} h(z) e^{x \rho(z)} \dif z
  \]
  Deformi $ \gamma $ in un cammino con gli stessi estremi che passa per un punto $ c $ di sella per $ \rho $, e che sia un cammino di steepest descent per $ \rho $, più altri eventuali pezzi che devi trattare a mano. A questo punto sul cammino di steepest descent $ \operatorname{\mathfrak{Im}}\rho $ è costante e può essere portata fuori, e il resto dell'integrale riparametrizzato a un integrale reale che tratti con Laplace più contributi di bordo.

\item \textbf{Punti mobili:}
  Se, in uno dei tre casi precedenti, l'esponente non è della forma giusta ma della forma $ f(x,t) $ puoi provare a trovare un punto critico $ \hat{t}(x) $ tale per cui $ \eval[1]{\pd{f}{t}}_{t = \hat{t}} = 0 $,fare il cambio di variabile $ t = \tau \hat{t} $, e sperare che funzioni. In generale ti aspetti che funzioni se espandendo $ f $ al secondo ordine in $ t $ attorno a $ \hat{t} $, il coefficiente del secondo ordine è negativo e cresce in modulo con $ x $.

\item \textbf{Metodi a caso per bassi $ \beta $:}
  Espandere in Taylor nell'integrale, a patto che la funzione che moltiplica $ \beta $ nell'esponente sia limitata; oppure introdurre un cutoff a caso e trovare solo i pezzi che divergono. Se hai una serie è legittimo approssimarla come integrale perché gli errori relativi tendono a zero.
\end{itemize}

\subsection{Derivate parziali e notazione compatta per gli Jacobiani}

Variabili ripetute:
\[ \pd{(A,B)}{(A,C)} =  \pf{B}{C}{A} \]

\subsection{Formule dello Huang}
Sia $ f(x,y,z) = 0 $. Allora valgono:
\begin{itemize}
\item Se $ w $ è una funzione del punto sulla varietà, ovvero di due delle tre variabili,
  \[ \pf{x}{y}{w} \pf{y}{z}{w} = \pf{x}{z}{w} \]

\item Si ha:
  \[ \pf{x}{y}{z} = \frac{1}{\pf{y}{x}{z}} \]

\item Regola della catena:
  \[ \pf{x}{y}{z} \pf{y}{z}{x} \pf{z}{x}{y} = -1 \]
  Questa si dovrebbe generalizzare a $ f(x_1, \dots, x_n) $ come
  \[ \prod_{i = 1}^n \pf{x_i}{x_{i+i}}{x_{i+2}, \dots, x_{i-1}} = (-1)^n \]
\end{itemize}

\section{Termodinamica}

Potenziali termodinamici:
\begin{align*}
  U(S, V, N) & \\
  H(S, p, N) &= U + pV \\
  A(T, V, N) &= U - TS \\
  G(T, p, N) &= U - TS + pV = N \mu (T, p) \\
  \Omega(T, V, \mu) &= U - TS - \mu N = - V p(T, \mu)
\end{align*}
Differenziali:
\begin{align*}
  \dif U &= T \dif S - p \dif V + \mu \dif N \\
  \dif H &= T \dif S + V \dif p + \mu \dif N \\
  \dif A &= - S \dif T - p \dif V + \mu \dif N \\
  \dif G &= - S \dif T + V \dif p + \mu \dif N \\
  \dif \Omega &= - S\dif T - p \dif V - N \dif \mu
\end{align*}

\section{Calori specifici}

\[ C_V = \pf{U}{T}{V} = T \pf{S}{T}{V} \]
\[ C_p = \pf{H}{T}{p} = T \pf{S}{T}{p} \]

\subsection{Condizioni di equilibrio}

\begin{itemize}
\item Un qualunque sistema all'equilibrio a temperatura fissa e che non possa scambiare lavoro con l'esterno si trova in un minimo di $ A $.

\item Se in un sistema termodinamico il lavoro scambiato con l'esterno è dato da $ \dif W = X \dif Y $, allora posto $ \Phi = A - XY $, in uno stato di equilibrio con $ T $ ed $ X $ fisso, $ \Phi $ è minimo.
\end{itemize}

Per un sistema in equilbrio stabile
\begin{itemize}
\item $ T / C_V > 0 $
\item $ \pf{p}{V}{T} < 0 $
\item Se $ \alpha = \frac{1}{V} \pf{V}{T}{p} $ e $ \kappa_T = -\frac{1}{V} \pf{V}{p}{T} $, vale
  \[
    C_p - C_V =
    - T \pf{V}{p}{T} \sbr{\pf{p}{T}{V}}^2 =
    - T \pf{p}{V}{T} \sbr{\pf{V}{T}{p}}^2 =
    \frac{\alpha^2 T V}{\kappa_T}
  \]
\item Se $ \kappa_S = -\frac{1}{V} \pf{V}{p}{S} $, allora $ \frac{C_p}{C_V} = \frac{\kappa_T}{\kappa_S} $
\end{itemize}

\section{Meccanica Statistica Classica}

\subsection{Ensemble Microcanonico}

\[
  \Gamma_N(U) = \left[\frac{1}{N!}\right] \int \frac{\dif^{\,n} q \dif^{\,n} p}{h^n} \; \delta{\del{\frac{H(q_1, \dots, q_n, p_1, \dots, p_n)}{U} - 1}}
\]

\subsection{Ensemble Canonico}
\[
  Z = \left[\frac{1}{N!}\right] \int \frac{\dif^{\,n} q \dif^{\,n} p}{h^n} \; e^{-\beta H}
  \qquad
  U = -\pd{}{\beta} \log Z
\]


\[
  A = - \frac{1}{\beta} \log Z
  \qquad
  \frac{S}{k} = \beta U + \log Z
\]

\begin{itemize}
\item $ \dpd{\beta}{T} = -\dfrac{1}{kT^{2}} = -k\beta^{2} $ dove $ \beta = \dfrac{1}{kT} $.
\item Se l'hamiltoniana è fattorizzata $ H = H_{1} + H_{2} $ e invariante per scambio di $ 1 \to 2 $ allora
  \[ Z = Z_{1} \cdot Z_{2} \qquad U = U_{1} + U_{2} \qquad S = S_{1} + S_{2} \]

\item Se l'hamiltoniana è della forma $ H = \sum_{i=1}^{N} H_{1}(\vec{q}_{i}, \vec{p}_{i}) $ allora la funzione di partizione è $ Z = \dfrac{1}{N!} (Z_{1})^{N} $ dove
  \[
    Z_{1} = \int \frac{\dif^{\,d} q \dif^{\,d} p}{h^d} \; e^{-\beta H_{1}(\vec{q}, \vec{p})}
  \]
  Si ricava allora
  \[
    \frac{U}{N} = U_{1} = - \dpd{}{\beta} \log{Z_{1}}
  \]
  ma l'entropia ha una forma semplice solo nel limite termodinamico $ N\to+\infty $ a causa del fattore $ N! $
  \[
    \frac{S}{kN} \simeq \beta U_{1} + \log{\frac{Z_{1}}{N}} + 1
  \]
  Il potenziale chimico è
  \[
    \mu(N, T, V) = \left(\dpd{A}{N}\right)_{T, V} \simeq -\frac{1}{\beta} \dpd{}{N}\left(N \log{Z_{1}} - N\log{N} + N\right) = \frac{1}{\beta}\log{\frac{N}{Z_{1}}}
  \]
  La pressione è $ \dfrac{p}{N} = \dfrac{1}{\beta} \left(\dpd{\log{Z_{1}}}{V}\right)_{T} $

\item Se ho $ N $ particelle \emph{distinguibili} di cui l'hamiltoniana di singola particella $ H_{1} $ ha spettro discreto con livelli energetici $ \{E_{n}\}_{n\in I} $ e degenerazione $ \{d_{n}\}_{n\in I} $, la funzione di partizione è $ Z = (Z_{1})^{N} $ dove
  \[ Z_{1} = \mathrm{tr}\left[e^{-\beta H_{1}}\right] = \sum_{n\in I} d_{n} e^{-\beta E_{n}}\]
  Si ricava allora
  \[
    \frac{U}{N} = U_{1} = \langle H_{1} \rangle = \frac{\sum_{n\in I} d_{n} E_{n} e^{-\beta E_{n}}}{\sum_{n\in I} d_{n} e^{-\beta E_{n}}} = - \dpd{}{\beta} \log{Z_{1}}
  \]
  L'entropia è
  \[
    \frac{S}{kN} = \beta U_{1} + \log{Z_{1}}
  \]
  Il calore specifico a volume costante è proporzionale alla varianza dall'hamiltoniana
  \[
    \frac{C_{V}}{kN} = \langle (\Delta H_{1})^{2} \rangle = \left(\frac{\sum_{n\in I} d_{n} E_{n} e^{-\beta E_{n}}}{\sum_{n\in I} d_{n} e^{-\beta E_{n}}}\right)^{2} - \frac{\sum_{n\in I} d_{n} E_{n}^{2} e^{-\beta E_{n}}}{\sum_{n\in I} d_{n} e^{-\beta E_{n}}} = - \beta^{2} \dpd{U_{1}}{\beta}
  \]
\end{itemize}

\subsection{Ensemble Gran Canonico}
Funzione di gran partizione
\[ Q = \sum_{N}\sum_{i} e^{\beta \mu N} e^{-\beta E_{iN}} = \sum_{N} z^{N} Z_{N} \]
dove $ z = e^{\beta \mu} $ e $ Z_{N} $ è la funzione di partizione canonica a $ N $ corpi.

Relazioni notevoli:
\begin{gather*}
  \frac{S}{k} = \log Q - \mathcal{N} \log z + \beta U = \log Q - \beta \mu \mathcal{N} + \beta U \\
  U = - \del{\dpd{\log Q}{\beta}}_{z} \qquad
  \mathcal{N} = z \del{\dpd{\log Q}{z}}_{\beta} = \del{\dpd{\log Q}{\log z}}_{\beta} \qquad
  \log Q = \frac{pV}{kT}
\end{gather*}
\begin{itemize}
\item Se la funzione di partizione canonica a $ N $ corpi è della forma $ Z_{N} = \frac{1}{N!}(Z_{1})^{N} $ la funzione di gran partizione ha la forma semplice
  \[ Q = e^{z Z_{1}}. \]
  Il numero medio di particelle è
  \[
    \mathcal{N} = z \dpd{\log{Q}}{z} = z Z_{1} = \log{Q} \quad \Rightarrow \quad \mu = kT \log\left[\frac{\mathcal{N}}{Z_{1}}\right]
  \]
  L'energia interna è
  \[
    U = - \mathcal{N} \dpd{\log{Z_{1}}}{\beta} = \mathcal{N} U_{1}
  \]
  L'entropia per numero medio di particelle è
  \[
    \frac{S}{k \mathcal{N}} = \beta U_{1} + \log{z} + 1
  \]
  La pressione è semplicemente
  \[
    \frac{pV}{kT} = \log{Q} = \mathcal{N} \quad \Rightarrow \quad p = \frac{\mathcal{N} k T}{V}
  \]
\item Se la funzione di partizione canonica a $ N $ corpi è della forma $ Z_{N} = (Z_{1})^{N} $ allora la funzione di gran partizione ha la forma
  \[ Q = \frac{1}{1 - z Z_{1}}\]
  a patto che $ \abs{z Z_{1}} < 1 $. Il numero medio di particelle è
  \[
    \mathcal{N} = \frac{z Z_{1}}{1 - z Z_{1}} = Q - 1 \quad \Rightarrow \quad \mu = kT \log\left[\frac{\mathcal{N}}{Z_{1} (\mathcal{N} + 1)}\right]
  \]
  L'energia interna è
  \[
    U = - \mathcal{N} \dpd{\log{Z_{1}}}{\beta} = \mathcal{N} U_{1}
  \]
  L'entropia per numero medio di particelle è
  \[
    \frac{S}{k \mathcal{N}} = \frac{\log{(\mathcal{N} +1)}}{\mathcal{N}} + \log{\left(Z_{1} \frac{\mathcal{N} + 1}{\mathcal{N}}\right)} + \beta U_{1} \quad \longrightarrow \quad \log{Z_{1}} + \beta U_{1}
  \]
  dove si è fatto nel limite $ \mathcal{N} \to +\infty $. La varianza del numero di particelle è
  \[
    \langle(\Delta N)^{2}\rangle = z \dpd{\mathcal{N}}{z} = \mathcal{N} + \mathcal{N}^{2} \quad \Rightarrow \quad \frac{\sqrt{\langle(\Delta N)^{2}\rangle}}{\mathcal{N}} = \sqrt{1 + \frac{1}{\mathcal{N}}}
  \]
  quindi non è più vero che le fluttuazioni del numero medio di particelle tendono a zero nel limite di $ \mathcal{N} \to +\infty $.
\end{itemize}

\subsection{Sistemi noti}

\subsubsection{Gas perfetto (generalizzato)}
$ N $ particelle (indistinguibili) di massa $ m $ in $ d $ dimensioni con hamiltoniana $ H = \sum_{i=1}^{N} \alpha \abs{\vec{p}_{i}}^{\nu} $ in un volume $ V $.

\begin{itemize}
\item \emph{Microcanonico}:
  \[
    \Gamma_N(U) = \frac{1}{N! \, \Gamma(Nd/\nu)}\left[\frac{V \Omega_{d} \Gamma(d/\nu)}{h^{d} \nu} \left(\frac{U}{\alpha}\right)^{d/\nu}\right]^{N}
    \qquad
    \frac{S}{kN} \simeq 1 + \frac{d}{\nu} + \log{\left[\frac{V}{N}\frac{\Omega_{d} \Gamma(d/\nu)}{\nu h^{d}} \left(\frac{\nu}{\alpha d} \frac{U}{N}\right)^{d/\nu}\right]}
  \]
\item \emph{Canonico}:
  \[
    Z_{1} = \frac{V \Omega_{d}}{h^{d}} \frac{\Gamma(d/\nu)}{\nu} \frac{1}{(\beta \alpha)^{d/\nu}}
    \qquad
    U = N \frac{d}{\nu\beta}
    \qquad
    \frac{S}{kN} \simeq 1 + \frac{d}{\nu} + \log{\left[\frac{V}{N}\frac{\Omega_{d} \Gamma(d/\nu)}{\nu h^{d}} \frac{1}{(\beta \alpha)^{d/\nu}}\right]}
  \]
\item Equazione di stato: $ p V = N/\beta = N k T $
\item Calori specifici: $ \dfrac{C_{V}}{kN} = \dfrac{d}{\nu} $ e $ \dfrac{C_{P}}{kN} = 1 + \dfrac{d}{\nu} $
\end{itemize}
Nel caso usuale di gas di particelle libere in cui $ d = 3 $, $ \alpha = 1/(2m) $ e $ \nu=2 $ si ottiene
\[
  Z_{1} = \frac{V}{h^{3}} \left(\frac{2\pi m}{\beta}\right)^{3/2}
  \qquad
  U = N \frac{3}{2\beta}
  \qquad
  \frac{S}{kN} \simeq \frac{5}{2} + \log{\left[\frac{V}{N} \left(\frac{2\pi m}{h^{2}\beta}\right)^{3/2}\right]}
\]
L'espressione per l'entropia è detta formula di Sackur-Tetrode.

% Inserire densità di stati, funzione di partizione, entropia &co

\subsubsection{Cristallo o gas di oscillatori}
$ N $ particelle (distinguibili) di massa $ m $ in $  d $ dimensioni con hamiltoniana
\[ H = \sum_{i=1}^{N} \left(\frac{\abs{\vec{p}_{i}}^{2}}{2m} + \frac{m\omega^{2}}{2}\abs{\vec{q}_{i} - \vec{q}_{i, 0}}^{2}\right) \]
\begin{itemize}
\item \emph{Microcanonico}: \[ \Gamma_N(U) = \frac{1}{\Gamma(N d)} \left(\frac{U}{\hbar \omega}\right)^{N d} \qquad S = kN  d\left[1 + \log{\left(\frac{1}{ d}\frac{U}{N \hbar \omega}\right)}\right]\]
\item \emph{Canonico}: \[ Z_{1} = \frac{1}{(\beta \hbar \omega)^{ d}} \qquad U = N\frac{ d}{\beta} \qquad S = kN  d \left[1 - \log{(\beta h\bar \omega)}\right]\]
\item L'energia libera di Helmoltz $ A = N d\dfrac{\log{\beta \hbar \omega}}{\beta} $ è indipendente dal volume quindi $ p = -\left(\dpd{A}{V}\right)_{T} = 0 $.
\end{itemize}

\subsubsection{Gas relativistico}
\textcolor{red}{Mettere le $ c $ a giro} \\
$ N $ particelle (indistinguibili) di massa $ m $ in $ d $ dimensioni con hamiltoniana $ H = \sum_{i=1}^{N} \sqrt{m^{2} + \abs{\vec{p}_{i}}^{2}} $ in un volume $ V $. \\
Funzione di partizione
\[
  Z_{1} = \frac{V \Omega_{d} m^{d} }{h^{d} d} \frac{1}{(\beta m)^{(d-1)/2}} K_{\frac{d+1}{2}}(\beta m)
\]
dove abbiamo introdotto le funzioni di Bessel modificate
\[
  K_{\nu}(z) = z^{\nu} \int_{1}^{+\infty} (x^{2} - 1)^{z - \frac{2}{2}} e^{-zx} \dif{x}
\]
Energia interna
\[
  \frac{U}{N} = m \frac{K_{\frac{d-1}{2}(\beta m)}}{K_{\frac{d+1}{2}}(\beta m)} + \beta d
\]
Si hanno i seguenti limiti di temperatura
\[
  \frac{U}{N} \simeq
  \begin{dcases}
    m + \frac{d}{2\beta} & \text{per } \beta m \gg 1 \text{ (basse temperature)}  \\
    \frac{d}{\beta} & \text{per } \beta m \ll 1 \text{ (alte temperature)}
  \end{dcases}
\]
Intuitivamente (in 3 dimensioni) per basse temperature l'hamiltoniana di singola particella è quella classica è più l'energia a riposo $ H_{1} = m + \vec{\abs{p}}^{2}/(2m) $ da cui per il teorema di equipartizione $ U/N = m + (3kT)/2 $; per alte temperature le particelle sono ultra-relativistiche quindi l'hamiltoniana di singola particella è $ H_{1} = \abs{\vec{p}} $ quindi sempre per equipartizione, $ U/N = 3 k T $ alte.


\subsubsection{Sistema a due livelli}
$ N $ particelle distinguibili non interagenti con 2 stati di energia interna $ \pm \epsilon $ con $ \epsilon > 0 $.
\[
  Z_{1} = e^{-\beta \epsilon} + e^{\beta \epsilon} = 2\cosh{(\beta \epsilon)}
  \qquad
  \frac{U}{N} = - \epsilon \tanh(\beta \epsilon)
  \qquad
  \frac{S}{kN} = - \beta \epsilon \tanh{(\beta \epsilon)} + \log{(2 \cosh{\beta \epsilon})}
\]
Limiti di temperatura
\[
  \frac{U}{N} \simeq
  \begin{dcases}
    - \epsilon(1 - e^{-\beta \epsilon}) & \text{per } \beta \epsilon \gg 1 \text{ (basse temperature)} \\
    - \epsilon^{2} \beta & \text{per } \beta \epsilon \ll 1 \text{ (alte temperature)}
  \end{dcases}
\]

\subsubsection{Colonna di gas in campo gravitazionale uniforme}
$ N $ particelle indistinguibili di massa $ m $ confinate in una colonna verticale di altezza $ l $ e area di base $ a $ immerse in un campo gravitazionale costante nella direzione naturale (in 3 dimensioni). L'hamiltoniana di singola particella è
\[
  H_{1} = \frac{p_{x}^{2} + p_{y}^{2} + p_{z}^{2}}{2m} + m g z
\]
Si ha
\[
  Z_{1} = \frac{al}{h^{3}} \left(\frac{2\pi m}{\beta}\right)^{\frac{3}{2}} \frac{1 - e^{-\beta m g l}}{\beta m g l}
  \qquad
  \frac{U}{N} = \frac{5}{2\beta} - \frac{m g l}{e^{\beta m g l} - 1}
\]
Forza sul tappo superiore
\[
  f\ped{sup} = ap = - \left(\dpd{A}{l}\right)_{T} = \frac{1}{\beta} \left(\dpd{\log{Z_{1}}}{l}\right)_{T} = \frac{N m g}{e^{\beta m g l} - 1}
\]
Forza sul tappo inferiore (a meno di un segno globale)
\[
  f\ped{inf} = N m g + \frac{N m g}{e^{\beta m g l} - 1}
\]
Pressione sulle pareti laterali in funzione dell'altezza
\[
  p\ped{lat}(z) =
  \frac{N m g}{a} \frac{e^{- \beta m g z}}{1 - e^{- \beta m g l}} =
  \frac{N m g}{a} e^{- \beta m g z} \del{1 + \frac{1}{e^{\beta m g l} - 1}}
\]
Densità di particelle in funzione dell'altezza
\[
  \varrho(z) \dif{z} = \frac{1}{l} \frac{\beta m g l e^{-\beta m g z}}{1- e^{-\beta m g l}} = \frac{\alpha}{1 - e^{-\alpha}}  e^{-\alpha z/l} \, \frac{\dif{z}}{l}
\]
La pressione è funzione dell'altezza: $ p(z) = \frac{kT}{al} N(z) = \frac{N kT}{al} \varrho(z) $. \\

Limiti di temperatura
\begin{align*}
  \frac{U}{N} & \simeq
                \begin{dcases}
                  \frac{5}{2\beta} - mgl e^{-\beta m g l} & \text{per } \beta mgl \gg 1 \text{ (basse temperature)} \\
                  \frac{3}{2\beta} & \text{per } \beta mgl \ll 1 \text{ (alte temperature)}
                \end{dcases} \\
  \varrho(z) & \simeq
               \begin{dcases}
                 \frac{\beta mg l}{l}\, e^{-\beta m g z} & \text{per } \beta mgl \gg 1 \text{ (basse temperature)} \\
                 \frac{1}{l} (1 - \beta m g z) & \text{per } \beta mgl \ll 1 \text{ (alte temperature)}
               \end{dcases}
\end{align*}

\section{Meccanica Quantistica}

\subsection{WKB}

Formule di connessione\footnote{Codice per le formule seguenti gentilmente fornito da Giuseppe Bogna, \url{https://uz.sns.it/~BeppeBogna}} per un punto di inversione destro $ x^* $

\begin{align*}
  \frac{2}{\sqrt{p(x)}}
  \cos\left(\frac{1}{\hbar}\int_{x}^{x^*}p(\xi)\dif \xi-\frac{\pi}{4}\right)
  &\longleftrightarrow
    \frac{1}{\sqrt{|p(x)|}}
    \exp\left(-\frac{1}{\hbar}\int_{x^*}^{x}|p(\xi)|\dif\xi\right)\\
  \frac{1}{\sqrt{p(x)}}
  \exp\left(\frac{i}{\hbar}\int_{x}^{x^*}p(\xi)\dif \xi-\frac{\pi}{4}\right)
  &\longleftrightarrow
    \frac{1}{i\sqrt{|p(x)|}}\exp\left(\frac{1}{\hbar}\int_{x^*}^{x}|p(\xi)|\dif \xi\right)\\
  -\frac{1}{\sqrt{p(x)}}\sin\left(\frac{1}{\hbar}\int_{x}^{x^*}p(\xi)\dif \xi-\frac{\pi}{4}\right)
  &\longleftrightarrow
    \frac{1}{\sqrt{|p(x)|}}\exp\left(\frac{1}{\hbar}\int_{x^*}^{x}|p(\xi)|\dif\xi\right)
\end{align*}

Formule di connessione per un punto di inversione sinistro $ x^* $
\begin{align*}
  \frac{1}{\sqrt{|p(x)|}}
  \exp\left(-\frac{1}{\hbar}\int_{x}^{x^*}|p(\xi)|\dif\xi\right)
  &\longleftrightarrow
    \frac{2}{\sqrt{p(x)}}
    \cos\left(\frac{1}{\hbar}\int_{x^*}^{x}p(\xi)\dif \xi-\frac{\pi}{4}\right)\\
  \frac{1}{i\sqrt{|p(x)|}}\exp\left(\frac{1}{\hbar}\int_{x}^{x^*}|p(\xi)|\dif \xi\right)
  &\longleftrightarrow
    \frac{1}{\sqrt{p(x)}}
    \exp\left(\frac{i}{\hbar}\int_{x^*}^{x}p(\xi)\dif \xi-\frac{\pi}{4}\right)\\
  \frac{1}{\sqrt{|p(x)|}}\exp\left(\frac{1}{\hbar}\int_{x}^{x^*}|p(\xi)|\dif\xi\right)
  &\longleftrightarrow
    -\frac{1}{\sqrt{p(x)}}\sin\left(\frac{1}{\hbar}\int_{x^*}^{x}p(\xi)\dif \xi-\frac{\pi}{4}\right)\\
\end{align*}

\section{Meccanica statistica quantistica}
Per un gas quantistico in generale vale
\[
  Q = \prod_{\alpha} \left[\sum_{n_{\alpha}} z^{n_{\alpha}} e^{-\beta n_{\alpha} E_{\alpha}}\right]^{d_{\alpha}}
\]
dove $ \alpha $ indicizza gli stati, $ d_{\alpha} $ è la degenerazione, $ n_{\alpha} $ sono i possibili numeri di occupazione dello stato $ \alpha $ e $ E_{\alpha} $ è l'energia di tale stato. Per i \emph{fermioni} vale $ n_{\alpha} = 0, 1 $ per ogni $ \alpha $, mentre per i \emph{bosoni} è $ \alpha \in \N $. Pertanto per \emph{fermioni} (segno superiore) e per i \emph{bosoni} (segno inferiore) di spin $ s $ vale
\begin{gather*}
  \log{Q} = \pm (2s+1)\sum_{\alpha}\log\left(1 \pm z e^{-\beta E_{\alpha}}\right) \\
  \mathcal{N} = (2s+1)\sum_{\alpha} \frac{1}{\frac{1}{z} e^{\beta E_{\alpha}} \pm 1}
  \qquad
  U = (2s+1)\sum_{\alpha} \frac{E_{\alpha}}{\frac{1}{z} e^{\beta E_{\alpha}} \pm 1}
\end{gather*}

Nel caso in cui il numero di stati accessibili sia illimitati e nel limite di volume infinito, possiamo far diventare la somma un integrale e integrare sullo spazio delle fasi classico ottenendo
\[
  \mathcal{N} = (2s+1) \int \frac{\dif^{\,d} q \dif^{\,d} p}{h^d} \frac{1}{\frac{1}{z} e^{\beta H} \pm 1}
  \qquad
  U = \mathcal{N} = (2s+1) \int \frac{\dif^{\,d} q \dif^{\,d} p}{h^d} \frac{H}{\frac{1}{z} e^{\beta H} \pm 1}
\]
e similmente per $ \log{Q} $. \\

A volte capita che sia nota la densità di occupazione degli stati in funzione dell'energia $ \epsilon $. In tale caso usare
\[
  \mathcal{N} = \int_{0}^{+\infty} \frac{g(\epsilon)}{\frac{1}{z}e^{\beta (\epsilon - \mu)} \pm 1} \dif{\epsilon}
  \qquad
  U = \int_{0}^{+\infty} \frac{\epsilon g(\epsilon)}{e^{\beta (\epsilon- \mu)} \pm 1} \dif{\epsilon}
\]

\subsection{Integrale magico}
\begin{equation}
  I^{\pm}_{\lambda}(z) = \int_{0}^{+\infty} \frac{x^{\lambda - 1}}{\frac{1}{z}e^{x} \pm 1}
\end{equation}
\begin{itemize}
\item Per $ \abs{z} < 1 $ (quindi in particolare per $ z \to 0 $) vale
  \begin{equation}\label{eq:im-serie}
    I_{\lambda}^{\pm}(z) = \Gamma(\lambda) \sum_{n=1}^{+\infty} (\mp1)^{n-1} \frac{z^{n}}{n^{\lambda}} = \Gamma(\lambda) \left[z \mp \frac{z^{2}}{2^{\lambda}} + O(z^{3})\right]
  \end{equation}
\item Per $ z\to +\infty $, $ I^{+}_{\lambda}(z) $ diverge. L'andamento asintotico è
  \[
    I_{\lambda}^{+}(z) = \frac{(\log{z})^{\lambda}}{\lambda} \left[1 + \frac{\pi^{2}}{6} \frac{\lambda(\lambda-1)}{(\log{z})^{2}} + \textcolor{red}{O\left(\frac{1}{z^{2}}\right)}\right]
  \]
\item Per $ \lambda > 1 $ la serie in~\eqref{eq:im-serie} converge anche per $ z=1 $ quindi
  \[
    I_{\lambda}^{\pm}(1) = \lim_{z\to1} \Gamma(\lambda) \sum_{n=1}^{+\infty} (\mp1)^{n-1} \frac{z^{n}}{n^{\lambda}}
    = \Gamma(\lambda) \sum_{n=1}^{+\infty} \frac{(\mp1)^{n-1}}{n^{\lambda}} =
    \begin{dcases}
      \Gamma(\lambda) \zeta(\lambda)\left(1 - 2^{1-\lambda}\right) & \text{per } I^{+} \\
      \Gamma(\lambda) \zeta(\lambda) &\text{per } I^{-}
    \end{dcases}
  \]
\item Per $ \lambda = 1 $ vale per $ z\to1 $
  \[
    I^{-}_{\lambda}(z) \sim -\log{\abs{1 - z}}
  \]
\item Per $ 0 < \lambda < 1 $ vale per $ z\to 1 $
  \[
    I^{-}_{\lambda}(z) \sim \frac{1}{(1-z)^{1-\lambda}} \frac{\pi}{\sin(\lambda\pi)}
  \]
\end{itemize}

\subsection{Tecniche generali}

\begin{itemize}
\item Fermioni a basse temperature nel limite del continuo: approssima il denominatore come una $ \theta $, fai il conto e tira fuori $ \varepsilon_F $. Per le correzioni integra per parti la formula per $ \mathcal{N} $, il che ti aspetti fornisca un oggetto simile a una $ \delta $, e approssima da qui.

  Formule utili a bassa temperatura
  \begin{gather*}
    \mathcal{N} = \mathrm{tr}\left[\theta(\mathcal{E}\ped{F} - H)\right] \qquad
    U = \mathrm{tr}\left[H\theta(\mathcal{E}\ped{F} - H)\right] \\
    \log{Q} = \mathrm{tr}\left[\beta(\mathcal{E}\ped{F} - H)\theta(\mathcal{E}_{F} - H)\right] = \beta \mathcal{E}\ped{F} \mathcal{N} - \beta U
  \end{gather*}
  Da cui troviamo che l'entropia a basse temperature è nulla e quindi si può usare $ p = - \left(\dpd{U}{V}\right)_{\mathcal{N}} $. Se non mi fido posso invece usare
  \[
    p = \frac{\log{Q}}{\beta V} = \mathcal{E}\ped{F}\frac{\mathcal{N}}{V} - \frac{U}{V} = \mathcal{E}\ped{F} n - u
  \]
\item Fermioni a basse temperature non nel limite del continuo\footnote{Si applica anche al caso in cui hai bande continue ma sei in mezzo tra due bande}: La cosa sopra fallisce perché non determina univocamente $ \varepsilon_F $, ma si sistema uguagliando le formule per $ \mathcal{N} $ a $ T=0 $ e a basse temperature e poi prendendo il limite $ T \to 0 $
\item Bosoni a basse temperature nel limite del continuo: Adimensionalizzi l'integrale per $ \mathcal{N} $, vedi che è monotono in $ z $ e che di fronte hai un coefficiente che va a zero con $ T $ e quindi claimi che $ z $ deve tendere al suo valore limite.
\item Alte temperature nel limite del continuo: claimi che $ z \ll 1 $, espandi, vedi a posteriori che era vero e lo inverti all'ordine giusto.
\end{itemize}

\subsection{Gas quantistici liberi}

Consideriamo un gas di $ \mathcal{N} $ fermioni o bosoni in $ d $ dimensioni con Hamiltoniana $ H = \alpha \envert{p}^{\nu} $, in un volume $ V $. Definiamo
\[
  n_Q(T) = \del{\frac{\pi^{\nu / 2}}{\beta\alpha h^{\nu}}}^{\frac{d}{\nu}}
  \qquad
  \gamma = \frac{2}{\nu} \frac{\Gamma\del{\frac{d}{\nu}}}{\Gamma\del{\frac{d}{2}}}
\]
ed indichiamo con $ g_s $ la degenerazione dovuta allo spin. Per \emph{fermioni} usiamo il segno superiore e per i \emph{bosoni} il segno inferiore. \\

In ogni regime valgono le seguenti espressioni
\begin{align*}
  n &= \frac{\mathcal{N}}{V} = g_{s}
      \frac{\Omega_d}{h^d} \frac{1}{\nu} \del{\alpha\beta}^{- \frac{d}{\nu}}
      I_{\frac{d}{\nu}}^{\pm}(z) = g_s n_{Q}(T) \frac{2}{\nu} \frac{1}{\Gamma(d/\nu)}
      I_{\frac{d}{\nu}}^{\pm}(z) \\
  \beta u &= \beta \frac{U}{V} = g_{s}
            \frac{\Omega_d}{h^d} \frac{1}{\nu} \del{\alpha\beta}^{- \frac{d}{\nu}}
            I_{\frac{d}{\nu} + 1}^{\pm}(z) = g_s n_{Q}(T) \frac{2}{\nu} \frac{1}{\Gamma(d/\nu)}
            I_{\frac{d}{\nu} + 1}^{\pm}(z)
\end{align*}
E inoltre vale
\[
  pV = \frac{\nu}{d} U
\]


\subsubsection{Limite di alte temperature o bassa fugacità}
In questo regime (limite semiclassico) otteniamo
\[
  z = \frac{1}{\gamma g_s} \frac{n}{n_Q} \pm \frac{1}{2^{d / \nu}} \del{\frac{1}{\gamma g_s} \frac{n}{n_Q}}^2 + \mathcal{O}\del{\del{\frac{n}{n_Q}}^3}
\]
\[
  U = \frac{d}{\nu} \mathcal{N} k T
\]

\subsubsection{Condensazione di Bose--Einstein}

Avviene quando $ d / \nu > 1 $ per Bosoni liberi.
La temperatura critica è quindi definita ponendo $ z=1 $ nell'espressione per $ \mathcal{N} $\footnote{Caso particolare $ d = 3 $, $ \nu = 2 $ da \[ T_c = \frac{h^2}{2\pi m k} \del{\frac{n}{(2s+1)\zeta\del{\frac{3}{2}}}}^{2/3} \]}

\[
  \mathcal{N} = V g_s \del{\frac{\pi^{\nu / 2}}{\beta_c\alpha h^{\nu}}}^{\frac{d}{\nu}}
  \frac{2}{\nu} \frac{\Gamma\del{\frac{d}{\nu}}}{\Gamma\del{\frac{d}{2}}}
  \zeta\del{\frac{d}{\nu}}
\]
Mentre il numero di occupazione del fondamentale è
\[
  n_0 = n \sbr{1 - \del{\frac{T}{T_c}}^{\frac{d}{\nu}}}
\]

\subsubsection{Gas di Fermi degenere relativistico}
L'hamiltoniana è $ H = \sqrt{m^{2}c^{4} + c^{2} \abs{\vec{p}}^{2}} $. Definendo impulso di Fermi come quello che risolve $ \mathcal{E}\ped{F}^{2} = m^{2} c^{4} + c^{2} p\ped{F}^{2} $, trovo $ \mathcal{N} $ e $ U $ facendo gli integrali sullo spazio delle fasi classico approssimando l'integrando con una $ \theta(p\ped{F} - p) $. Posto $ x\ped{F} = p\ped{F}/(mc) $ si trova usando $ x\ped{F} \gg 1 $
\[
  x\ped{F} = \frac{\hbar}{mc}(3\pi^{2} n)^{\frac{1}{3}}
  \qquad
  U \simeq \frac{V}{\pi^{2}} \frac{m^{4}c^{5}}{4\hbar^{3}}\left(x\ped{F}^{4} + x\ped{F}^{2}\right)
  \qquad
  p = - \dpd{U}{V} \simeq \frac{1}{12\pi^{2} \hbar^{3}} \left(x\ped{F}^{4} - x\ped{F}^{2}\right)
\]

\end{document}
